package com.bullapse.lottoorganizer.app;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bullapse.lottoorganizer.app.R;

public class AddLottoTicket extends Activity{

    //Class Variables
    String barcodeID;
    String barcodeLocation;
    String name;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lotto_ticket);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_lotto_ticket, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getBarcodeID(View view) {
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "SCAN_ALL");
            startActivityForResult(intent, 0);

        } catch (ActivityNotFoundException e) {
            tvMessage.setText("You need to download the scanner api");
            tvMessage.setTextColor(getResources().getColor(R.color.Red));
            //testing


        }
    }

    public void getBarcodeLocation(View view) {
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "SCAN_ALL");
            startActivityForResult(intent, 0);

        } catch (ActivityNotFoundException e) {
            tvMessage.setText("You need to download the scanner api");
            tvMessage.setTextColor(getResources().getColor(R.color.Red));
            //testing

        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                //Check to see if this is a Location [QR CODE] or a Lotto Ticket
                if (format.equals("QR_CODE")) { //[QR CODE]
                    barcodeLocation = intent.getStringExtra("SCAN_RESULT");
                    //We might want to do a parse int function here....Not too sure yet
                    EditText location = (EditText) findViewById(R.id.barcode_location);
                    location.setText(barcodeLocation);
                } else { //[Lotto Ticket]
                    barcodeID = intent.getStringExtra("SCAN_RESULT");
                    EditText id = (EditText) findViewById(R.id.barcode_id);
                    id.setText(barcodeID);
                }

            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
//                tvMessage.setText("Scan Cancelled");
//                tvMessage.setTextColor(getResources().getColor(R.color.Yellow));

            }
        }
    }


    public void saveLottoTicketInfo(View view) {
        EditText displayName = (EditText) findViewById(R.id.display_name);
        name = displayName.getText().toString();
        TextView tvCheckLottoTicketValues = (TextView) findViewById(R.id.tvCheckLottoTicketValues);
        tvCheckLottoTicketValues.setText("");
        if (barcodeID == null || barcodeID.equals("")|| barcodeLocation == null || barcodeLocation.equals("")|| name == null || name.equals("")) {
            String message = "";
            if (barcodeID == null || barcodeID.equals(""))
                message += "\"Barcode\" is Required\n";
            if (barcodeLocation == null || barcodeLocation.equals(""))
                message += "\"Barcode (Location)\" is Required\n";
            if (name == null || name.equals(""))
                message += "\"Display Name\" is Required\n";
            tvCheckLottoTicketValues.setText(message);
        } else {
            //LottoTicket newTicket = new LottoTicket(barcodeID, barcodeLocation, name);
            //System.out.println("BarcodeID: " + newTicket.getBarcode());
            System.out.println("BarcodeLocation: " + barcodeLocation);
            System.out.println("Name: " + name);
            //do something with the LottoTicket Object

            //Start Main Activity
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }
}
