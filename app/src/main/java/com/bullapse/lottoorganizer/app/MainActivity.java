package com.bullapse.lottoorganizer.app;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;


import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends FragmentActivity {

    private ArrayList<String> scannedBarcodes;
    private List<LottoTicket> lottoTickets;
    private DataSourceHelper dataSourceHelper;
    private allTicketsDatabaseHelper allTicketsDatabaseHelper;

    // for DEBUGGING
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String ZXING_PACKAGE ="com.google.zxing.client.android.SCAN";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent zxing = new Intent(ZXING_PACKAGE);
        if (zxing == null) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Create and show the dialog.
            MyDialogFragment newFragment = new MyDialogFragment ();
            newFragment.show(ft, "dialog");
        }
        scannedBarcodes = new ArrayList<String>();
        lottoTickets = new ArrayList<LottoTicket>();

        dataSourceHelper = new DataSourceHelper(this, null, null ,1);
        dataSourceHelper.open();

        allTicketsDatabaseHelper = new allTicketsDatabaseHelper(this);
        try {
            allTicketsDatabaseHelper.createDataBase();
            allTicketsDatabaseHelper.openDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }




    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            Intent intent = new Intent(this, AddLottoTicket.class);
//            startActivity(intent);
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void onClick(View v) {
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "SCAN_ALL");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException e) {
            tvMessage.setText("You need to download the scanner api");
            tvMessage.setTextColor(getResources().getColor(R.color.Red));
        }
    }


    private LottoTicket createLottoTicketObject(List<String> codes, int locID, int lottoID) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();


        String barcode = codes.get(lottoID);
        String location = codes.get(locID);
        System.out.println(lottoID);
        LottoTicket newTicket = allTicketsDatabaseHelper.getTicketMatchingID(barcode, location, dateFormat.format(date));

        return newTicket;
    }



    public void addLottoTicket(View view) {
        Intent intent = new Intent(this, AddLottoTicket.class);
        startActivity(intent);
    }

    public void takeInventory(View view) {
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);

        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.initiateScan();

        scannedBarcodes = new ArrayList<String>();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                   // Handle cancel
                Log.d(TAG, "RESULT_CANCELED");
                if (scannedBarcodes.size() == 0) {
                    Log.d(TAG, "scannedBarcode.size() == 0");
                    tvMessage.setText("Scan Cancelled");
                    tvMessage.setTextColor(getResources().getColor(R.color.Orange));
                } else if (scannedBarcodes.size() % 2 == 1) { //the list was missing a barcode. There should always be a UPC and a QR code for every Lotto Ticket
                    Log.d(TAG, "scannedBarcode.size() = " + scannedBarcodes.size() + "\t" + (scannedBarcodes.size() % 2 == 1));
                    tvMessage.setText("You missed a barcode!\nMake sure that you scan the location(QR Code) and Scratch-off(UPC barcode) for every Scratch off ");
                    tvMessage.setTextColor(getResources().getColor(R.color.Orange));
                } else {

                    for (int index = 0; index < scannedBarcodes.size(); index += 2) {
                          LottoTicket tempTicket = createLottoTicketObject(scannedBarcodes, index, index + 1);
                          dataSourceHelper.createTicket(tempTicket);
                    }
                    tvMessage.setText("LottoTickets successfully scanned: " + scannedBarcodes.size()/2);
                    tvMessage.setTextColor(getResources().getColor(R.color.Green));

                }

                }
                else
                {
                    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                    String code = scanResult.getContents();

                    scannedBarcodes.add(code);
                    Log.d(TAG, "code : " + code + " was successfully added to the arraylist.");
                    Log.d(TAG, "Number of scanned barcodes : " + scannedBarcodes.size());

                    IntentIntegrator intentIntegrator = new IntentIntegrator(MainActivity.this);
                    intentIntegrator.initiateScan();
                }
                break;
            }
        }
    }

    public void checkInventory(View view) {
        Intent intent = new Intent(this, DataBaseViewActivity.class);
        startActivity(intent);
    }

    /**
     * Abstract Class to create the create a database for the cards
     */

    public DataSourceHelper getDataSource() {
        return dataSourceHelper;
    }


}
