package com.bullapse.lottoorganizer.app;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Date;

/**
 * This is a LottoTicket Object
 * Created by Spencer Bull on 8/2/2014.
 */
public class LottoTicket implements Parcelable {

    private static final String TAG = MainActivity.class.getSimpleName();

    //class variables
    private long rowID;
    private String barcode;
    private String name;
    private String location;
    private String cost;
    private int texasLottoID;
    private int packSize;
    private String packCost;
    private String comments;
    private String dateTime;

    //Create an empty lottoTicket object
    public LottoTicket() {

    }
    /**
     * Create a LottoTicket object
     * @param c     --cannot be null
     * @param TLid  --cannot be null
     * @param n     --cannot be null
     * @param ps    --cannot be null
     * @param pc    --cannot be null
     * @param bc    --cannot be null
     * @param cmt   --cannot be null
     */
    public LottoTicket(String c, int TLid, String n, int ps, String pc, String bc, String l, String cmt, String date) {
        //check for nulls
        if (c != null || n != null  || pc != null || bc != null || cmt != null) {
            cost = c;
            texasLottoID = TLid;
            name = n;
            packSize = ps;
            packCost = pc;
            barcode = bc;
            location = l;
            comments = cmt;
            dateTime = date;
        } else
            Log.d(TAG, "one of the perameters was null.");

    }

    //Cost
    public String getCost() {return cost;}

    public void setCost(String c) {cost = c;}
    //Texas Lotto ID
    public int getTexasLottoID() {return texasLottoID;}

    public void setTexasLottoID(int lID) {texasLottoID = lID;}
    //Game Name
    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name; //temp
    }
    //Pack Size
    public int getPackSize() {return packSize;}

    public void setPackSize(int ps) {packSize = ps;}

    //Pack Cost
    public String getPackCost() {return packCost;}

    public void setPackCost(String pc) {packCost = pc;}

    //UPC
    public void setBarcode(String b) {barcode = b;}

    public String getBarcode() {return barcode;}
    //Comments
    public String getComments() {return comments;}

    public void setComments(String c) {comments = c;}

    //Location
    public String getLocation() {return location;}

    public void setLocation(String barcodeID) {location = barcodeID;}
    // DateTimeScanned
    public String getDateTime() {return dateTime;}

    public void setDateTime(String d) {dateTime = d;}
    // For Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.getBarcode());
        dest.writeString(this.getLocation());
        dest.writeString(this.getName());
    }

    private void readFromParcel(Parcel in) {
        this.setBarcode(in .readString());
        this.setLocation(in .readString());
        this.setName(in . readString());
    }


    //Override toString
    @Override
    public String toString() {
       return "Name: " + name +
               "\nCost: " + cost +
               "\nTexas Lotto ID: " + texasLottoID +
               "\nPack Size: " + packSize +
               "\nPack Cost: " + packCost +
               "\nUPC: " + barcode +
               "\nLocation: " + location +
               "\nComments: " + comments +
               "\nScan Date: " + dateTime;
    }

}
