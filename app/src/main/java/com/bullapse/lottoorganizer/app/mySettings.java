package com.bullapse.lottoorganizer.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.bullapse.lottoorganizer.app.R;

public class mySettings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_settings);
    }

    public void addLottoTicket(View view) {
        Intent intent = new Intent(this, AddLottoTicket.class);
        startActivity(intent);
    }
}
