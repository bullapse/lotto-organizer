package com.bullapse.lottoorganizer.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Spencer Bull on 8/7/2014.
 */
public class DataSourceHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "lottoTickets.db";

    public  static final String KEY_ROW = "RowID";
    public  static final String KEY_COST = "Cost";
    public  static final String KEY_LOTTO_ID = "LottoID";
    public  static final String KEY_GAME = "Game";
    public  static final String KEY_PACKSIZE = "PackSize";
    public  static final String KEY_PACKCOST = "PackCost";
    public  static final String KEY_UPC = "UPC";
    public  static final String KEY_LOCATION = "Location";
    public  static final String COMMENTS = "Comments";
    public  static final String DATE_TIME = "ScanDateTime";

    public  static final String TABLE_NAME = "lottoTickets";
    private static final String DICTIONARY_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    KEY_ROW + " INTEGER PRIMARY KEY, " +
                    KEY_COST + " TEXT," +
                    KEY_LOTTO_ID + " INTEGER, " +
                    KEY_GAME + " TEXT, " +
                    KEY_PACKSIZE + " INTEGER, " +
                    KEY_PACKCOST + " TEXT, " +
                    KEY_UPC + " TEXT, " +
                    KEY_LOCATION + " TEXT, " +
                    COMMENTS + " TEXT," +
                    DATE_TIME + " TEXT);";


    private String[] allColumns = {
            KEY_ROW,
            KEY_COST,
            KEY_LOTTO_ID,
            KEY_GAME,
            KEY_PACKSIZE,
            KEY_PACKCOST,
            KEY_UPC,
            KEY_LOCATION,
            COMMENTS,
            DATE_TIME

            //DataSourceHelper.KEY_PACK,
            //DataSourceHelper.KEY_NUMBER,
            //DataSourceHelper.KEY_PRICE
    };

    DataSourceHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() throws SQLException {
        this.getWritableDatabase();
    }

    public void close() {
        this.getWritableDatabase().close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DICTIONARY_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DataSourceHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void createTicket(LottoTicket ticket) {
        ContentValues values = new ContentValues();
        values.put(KEY_COST, ticket.getCost());
        values.put(KEY_LOTTO_ID, ticket.getTexasLottoID());
        values.put(KEY_GAME, ticket.getName());
        values.put(KEY_PACKSIZE, ticket.getPackSize());
        values.put(KEY_PACKCOST, ticket.getPackCost());
        values.put(KEY_UPC, ticket.getBarcode());
        values.put(KEY_LOCATION, ticket.getLocation());
        values.put(COMMENTS, ticket.getComments());
        values.put(DATE_TIME, ticket.getDateTime());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_NAME, null, values);
        db.close();


    }

    public boolean deleteTicket(String UPC) {
        boolean result = false;

        String query = "Select * FROM " + TABLE_NAME + " WHERE " + KEY_UPC + " =  \"" + UPC + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        LottoTicket ticket = new LottoTicket();

        if (cursor.moveToFirst()) {
            db.delete(TABLE_NAME, KEY_ROW + " = ?", null);

            cursor.close();
            result = true;
        }
        db.close();

        return result;
    }

    public List<LottoTicket> getAllTickets() {
        List<LottoTicket> comments = new ArrayList<LottoTicket>();


        Cursor cursor = this.getWritableDatabase().query(DataSourceHelper.TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            LottoTicket ticket = cursorToTicket(cursor);
            comments.add(ticket);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return comments;
    }

    private LottoTicket cursorToTicket(Cursor cursor) {
        return new LottoTicket(
                cursor.getString(1),
                cursor.getInt(2),
                cursor.getString(3),
                cursor.getInt(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9));
    }

}