package com.bullapse.lottoorganizer.app;

import android.app.ListActivity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class DataBaseViewActivity extends ListActivity {

    Context context = this;

    private DataSourceHelper datasource;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base_view);

        datasource = new DataSourceHelper(context, null, null, 1);

        db = datasource.getReadableDatabase();

        if (db == null) {
            List<String> values = new ArrayList<String>();
            values.add("No Data");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
            setListAdapter(adapter);
        } else {

            List<LottoTicket> values = datasource.getAllTickets();

            // use the SimpleCursorAdapter to show the
            // elements in a ListView
            ArrayAdapter<LottoTicket> adapter = new ArrayAdapter<LottoTicket>(this,
                    android.R.layout.simple_list_item_1, values);
            setListAdapter(adapter);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.data_base_view, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void setDatasource(DataSourceHelper newDataSource) {
        datasource = newDataSource;

    }
}
