This is the beginning of a larger app that can allow convenient store users to track the inventory of their lottery ticket. As of now, it creates a SQL-Light database directly on the Android Device. This will eventually be cloud based when I get the time to create a web-service and database. 
Note: 
This project uses the zxing project library that can be found here: 
zxing is an integrated bar-code scanning app that can read almost every style of bar-code. zxing is under the Apache License.